public class Application{
	public static void main(String... args) {
		Student youran=new Student();		
		youran.grade=93;
		youran.name="youran";
		youran.year=2023;
		System.out.println(youran.grade);
		System.out.println(youran.name);
		System.out.println(youran.year);
		youran.gradeName();
		
		Student Tommy=new Student();
		Tommy.grade=89;
		Tommy.name="Tommy";
		Tommy.year=2022;
		System.out.println(Tommy.grade);
		System.out.println(Tommy.name);
		System.out.println(Tommy.year);
		Tommy.gradeName();
	
		Student[] section3=new Student[3];
		section3[0]=youran;
		section3[1]=Tommy;
		section3[2]=new Student();
		section3[2].grade=85;
		section3[2].name="Noah";
		section3[2].year=2020;
		System.out.println(section3[0].grade);
		System.out.println(section3[2].grade);
	}
}